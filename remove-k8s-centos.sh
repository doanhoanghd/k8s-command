#!/bin/sh
set -x
sudo kubeadm reset --force
sudo yum remove -y kubeadm kubectl kubelet kubernetes-cni kube*
sudo yum autoremove -y
[ -e ~/.kube ] && sudo rm -rf ~/.kube
[ -e /etc/kubernetes ] && sudo rm -rf /etc/kubernetes
[ -e /opt/cni ] && sudo rm -rf /opt/cni

#Kubeadm unknown service runtime
sudo rm /etc/containerd/config.toml
sudo systemctl restart containerd
sudo kubeadm init
